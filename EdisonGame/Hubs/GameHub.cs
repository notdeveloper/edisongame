﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EdisonGame.Models.Identity;
using Microsoft.AspNet.SignalR;

namespace EdisonGame.Hubs
{
    public class GameHub : Hub
    {
        Dictionary<String, List<GameUser>> LobbiesGroup = new Dictionary<string, List<GameUser>>();

        public void SendToClients(string name, string message)
        {
            Clients.All.sendMessage(name, message);
        }

        public void InterLobby(string user, string lobby)
        {
            Clients.All.interLobbyClient(user, lobby);
        }

        public void Connect(string userName)
        {
            //Users.Add(new User { ConnectionId = id, Name = userName });

            // Посылаем сообщение текущему пользователю
            //Clients.Caller.onConnected(id, userName, Users);

            // Посылаем сообщение всем пользователям, кроме текущего
            //Clients.AllExcept(id).onNewUserConnected(id, userName);
        }
    }
}