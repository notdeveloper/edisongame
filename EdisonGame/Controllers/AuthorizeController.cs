﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EdisonGame.Models.Identity;
using EdisonGame.Models.GameModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace EdisonGame.Controllers
{
    [Authorize]
    public class AuthorizeController : Controller
    {
        private GameUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<GameUserManager>(); }
        }
        //
        // GET: /Home/
        [AllowAnonymous]
        public ActionResult LogIn(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();         
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel details, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                GameUser user = await UserManager.FindAsync(details.Name, details.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Invalid name or password.");
                }
                else
                {
                    ClaimsIdentity ident =
                        await UserManager.CreateIdentityAsync(user, 
                        DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties()
                    { IsPersistent = false }, ident);
                    OnlineUsersStorage.UserLogIn(user);
                    return Redirect("/");
                }
            }

            ViewBag.returnUrl = returnUrl;
            return View(details);
        }

        private IAuthenticationManager AuthManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }


        [AllowAnonymous]
        public ActionResult SignIn()
        {
            return View();
        }

        /// <summary>
        /// Создает пользователя с заданными характеристиками
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SignIn(UserCreateModel model)
        {
            if (ModelState.IsValid)
            {
                GameUser user = new GameUser() { UserName = model.Login, PublicName = model.PublicName };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn");
            }
        }

        public ActionResult LogOut()
        {
            GameUser user = UserManager.FindByName(HttpContext.User.Identity.Name);
            OnlineUsersStorage.UserLogOf(user);
            AuthManager.SignOut();            
            return RedirectToAction("LogIn");
        }
	}
}