﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EdisonGame.Hubs;
using EdisonGame.Models.GameModel;
using EdisonGame.Models.Identity;
using EdisonGame.Models.GameModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;

namespace EdisonGame.Controllers
{
    [System.Web.Mvc.Authorize]
    public class GameController : Controller
    {
        [HttpPost]
        public ActionResult CreateNewLobby(Lobby newLobby)
        {
            if (ModelState.IsValid)
            {
                newLobby.Creator = CurrentUser;
                if(newLobby.InGameAutomaticaly)
                    newLobby.Join(CurrentUser);

                GameContext.TheGame.Lobbies.Add(newLobby);

                var hubContext = GlobalHost.ConnectionManager.GetHubContext<GameHub>();
                hubContext.Clients.All.interLobbyClient(newLobby.Creator.PublicName, newLobby.LobbyName);

                return RedirectToAction("Index");
            }

            return View("CreateNewLobby");
        }

        public ActionResult CreateNewLobby()
        {
            Lobby lobby = new Lobby();
            return View(lobby);
        }

        public ActionResult Index()
        {
            GameGlobal game = GameContext.TheGame;
            game.AllUsers = HttpContext.GetOwinContext().GetUserManager<GameUserManager>().Users;
            game.OnlineUsers = OnlineUsersStorage.OnlineUsers;

            return View(game);
        }

        private GameUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<GameUserManager>(); }
        }

        private GameUser CurrentUser
        {
            get { return UserManager.FindByName(HttpContext.User.Identity.Name); }
        }
    }
}