﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EdisonGame.Models.Identity;

namespace EdisonGame.Models.GameModel
{
    public class OnlineUsersStorage
    {
        private static List<GameUser> _onlineUsers = new List<GameUser>();

        public static void UserLogIn(GameUser user)
        {
            if (!_onlineUsers.Contains(user))
                _onlineUsers.Add(user);
        }

        public static void UserLogOf(GameUser user)
        {
            _onlineUsers.Remove(user);
        }

        public static IEnumerable<GameUser> OnlineUsers
        {
            get { return _onlineUsers; }
        }
    }
}