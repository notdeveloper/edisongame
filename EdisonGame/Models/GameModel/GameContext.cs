﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdisonGame.Models.GameModel
{
    public class GameContext
    {
        public static GameGlobal TheGame { get; set; }

        static GameContext()
        {
            TheGame  = new GameGlobal();
        }
    }
}