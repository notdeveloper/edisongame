﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EdisonGame.Models.Identity;

namespace EdisonGame.Models.GameModel
{
    public class GameGlobal
    {
        public GameGlobal()
        {
            this.Lobbies = new List<Lobby>();
        }

        public IEnumerable<GameUser> AllUsers { get; set; }
        public IEnumerable<GameUser> OnlineUsers { get; set; }
        public List<Lobby> Lobbies { get; set; }
    }
}