﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EdisonGame.Models.Identity;

namespace EdisonGame.Models.GameModel
{
    public class Lobby
    {
        public Lobby()
        {
            this.Gamers = new List<GameUser>();
            LobbyName = "";
            LobbyCapacity = 6;
        }

        [Required]
        public String LobbyName { get; set; }
        [Required]
        public int LobbyCapacity { get; set; }
        [Required]
        public Boolean InGameAutomaticaly { get; set; }
        public List<GameUser> Gamers { get; set; }
        public GameUser Creator;

        public void Join(GameUser gamer)
        {
            if(!Gamers.Contains(gamer))
                Gamers.Add(gamer);
        }

        public void Exit(GameUser gamer)
        {
            Gamers.Remove(gamer);
        }
    }
}