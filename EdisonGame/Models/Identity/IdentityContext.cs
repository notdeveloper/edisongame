﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace EdisonGame.Models.Identity
{
    public class IdentityContext : IdentityDbContext<GameUser>
    {
        /// <summary>
        /// Конструктор с вызовом базового и параметром-именем строки подключения
        /// </summary>
        public IdentityContext() : base("IdentityDB"){}

        static IdentityContext()
        {
            Database.SetInitializer<IdentityContext>(new IdentityDbInit());
        }

        public static IdentityContext Create()
        {
            return new IdentityContext();
        }
    }
}