﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EdisonGame.Models.Identity
{
    public class UserCreateModel
    {
        [Required]
        public String PublicName { get; set; }
        [Required]
        public String Login { get; set; }
        [Required]
        public String Password { get; set; }
    }
}