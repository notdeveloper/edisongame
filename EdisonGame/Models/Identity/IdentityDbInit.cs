﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EdisonGame.Models.Identity
{
    public class IdentityDbInit
    : DropCreateDatabaseIfModelChanges<IdentityContext>
    {
        protected override void Seed(IdentityContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(IdentityContext context)
        {
            // Вот здесь сделаю начальную конфигурацию
        }
    }
}