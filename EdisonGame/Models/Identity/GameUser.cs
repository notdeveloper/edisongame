﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EdisonGame.Models.Identity
{
    public class GameUser : IdentityUser
    {
        // Может добавлю дополнительные поля
        public String PublicName { get; set; }
    }
}