﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace EdisonGame.Models.Identity
{
    public class GameUserManager: UserManager<GameUser>
    {

        public GameUserManager(IUserStore<GameUser> store)
            : base(store)
        {
            
        }
        
        public static GameUserManager Create(
            IdentityFactoryOptions<GameUserManager> options,
            IOwinContext context) 
        {
            IdentityContext db = context.Get<IdentityContext>();
            GameUserManager manager = new GameUserManager(new UserStore<GameUser>(db));
            return manager;
        }
    }
}